
const remplirQuestions = (count) => {
    cy.get("#generator_questionnaire_questions_"+count+"_title").type('Question '+count);
    cy.get("#generator_questionnaire_questions_"+count+"_content").type("Le soleil est-il beau ?");
    cy.get("#generator_questionnaire_questions_"+count+"_typeCognitif_2").click();
};

context('Admin', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8000/login');
      cy.get('#inputEmail').type('admin@elveo.com');
      cy.get('#inputPassword').type('elveo{enter}');
    })


    it('try generate questionnaire', () => {
        cy.visit('http://localhost:8000/admin/questionnaire/create');
        cy.get('#generator_questionnaire_title').type('Mon test cypress')

        remplirQuestions(0);
        cy.get('#generator_questionnaire_save').click();
        cy.wait(3000);    
        cy.get('article.col-12.center-v a:last-child').last().click();
    }
    )

    it('try update info', () => {
        cy.visit('http://localhost:8000/admin/account/edit');
        cy.get('#admin_firstName').type('Admin');
        cy.get('#admin_lastName').type('Elveo');
    })

    it('try create incub', () => {
        cy.visit('http://localhost:8000/admin/incubator/create');
        cy.get('#incubator_name').type('Incubator Cypress');
        cy.get('#incubator_street1').type('4 Rue Montparnasse');
        cy.get('#incubator_city').type('Paris');
        cy.get('#incubator_zipCode').type("75000");
        cy.get('#incubator_phoneNumber').type("0123456789{enter}");
    })

    it('try create entrepreneur', () => {
        cy.visit('http://localhost:8000/admin/user/create');
        cy.get('#user_email').type('cypress-elveo@yopmail.com');
        cy.get('#user_firstName').type('Cypress');
        cy.get('#user_lastName').type('Elveo');
        cy.get('#user_job').type('Automatic tester');
        cy.get('#user_phoneNumber').type('0123456789{enter}');
    });

})

context('Entrepreneur', () => {
    it('try answer questionnaire', () => {
        cy.visit('http://localhost:8000/login');
        cy.get('#inputEmail').type('entrepreneur1@elveo.com');
        cy.get('#inputPassword').type('elveo{enter}');

        cy.visit('http://localhost:8000/en/answer/answer-questionnaire/e2a45895ba5b1dddba5f55a7687d8ea6');
        for(var i = 0; i < 48 ; i++)
        {
            let random = (Math.random() * (6 - 1)) + 1;
            random = Math.floor(random);

            cy.get('#survey_answerQuestions_'+i+'_answer_'+random).scrollIntoView()
            .click();
        }
        cy.get('#survey_add').click();

        cy.wait(3000);
        let random = (Math.random() * (2 - 0)) + 0;
        random = Math.floor(random);
        cy.get('#description_description_'+random).click();
        cy.get('#cta-choose').click();
    })
})